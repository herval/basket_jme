//***********************************
// File    : Ball.java
// Created : 2003-06-24
// Modified: 2003-06-24
// 
// (c) 2003, Sollipsis Interactive
//***********************************

package com.sollipsis.basket;

import javax.microedition.lcdui.Graphics;

import com.sollipsis.sgf.Sprite;

import com.sollipsis.basket.config.Layout;


/**
 * A bola do jogo.
 *
 * @author Andrei de A. Formiga
 */
public class Ball extends Sprite
{
    private static final String[]	IMG_NAMES = { };
    
    public Ball()
    {
	super(Layout.BALL_W, Layout.BALL_H, IMG_NAMES);
    }
}
