//***********************************
// File    : Player.java
// Created : 2003-06-24
// Modified: 2003-06-25
// 
// (c) 2003, Sollipsis Interactive
//***********************************

package com.sollipsis.basket;


import javax.microedition.lcdui.Graphics;
import javax.microedition.lcdui.Image;

import com.sollipsis.sgf.util.ImageLoader;
import com.sollipsis.basket.config.Layout;


/**
 * Representa um jogador de basquete, com seus atributos e imagens.
 *
 * @author Andrei de A. Formiga
 */
public class Player
{
    //--- constantes ---
    public static final int	SKILL_MAX = 5;
    public static final int	STRENTH_MAX = 5;

    private static final int	STATE_STANDING = 0;
    private static final int	STATE_READY = 1;
    private static final int	STATE_THROWING = 2;

    private static final int	NUMBER_OF_STATES = 3;


    //--- variaveis de instancia ---
    private int			state;

    private int			strength;

    private int			skill;

    private Image[]		images = new Image[NUMBER_OF_STATES];


    public Player(int strength, int skill, String standingImgName, 
		  String readyImgName, String throwingImgName)
    {
	this.strength = strength;
	this.skill = skill;
	this.state = STATE_STANDING;
	
	images[STATE_STANDING] = ImageLoader.loadImage(standingImgName);
	images[STATE_READY] = ImageLoader.loadImage(readyImgName);
	images[STATE_THROWING] = ImageLoader.loadImage(throwingImgName);
    }

    public void paint(Graphics g)
    {
	g.drawImage(images[state], Layout.PLAYER_X, Layout.PLAYER_EY, 
		    Graphics.LEFT | Graphics.BOTTOM);
    }

    public void setState(int newState)
    {
	state = newState;
    }

    public int getStrength()
    {
	return strength;
    }

    public int getSkill()
    {
	return skill;
    }
}
