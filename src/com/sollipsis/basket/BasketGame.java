//***********************************
// File    : BasketGame.java
// Created : 2003-06-24
// Modified: 2003-06-24
// 
// (c) 2003, Sollipsis Interactive
//***********************************

package com.sollipsis.basket;

import com.sollipsis.sgf.Gamelet;
import com.sollipsis.sgf.GameCanvas;
import com.sollipsis.sgf.GameState;


/**
 * Classe principal do jogo "Basket".
 *
 * @author Andrei de A. Formiga
 */
public class BasketGame extends Gamelet
{
    //--- variaveis de instancia ---


    //--- interface publica ---
    public BasketGame()
    {
	gameCanvas = new BasketCanvas();
	playState = new GameState(gameCanvas);
	setActiveState(playState);
    }
    
    public String getGameName()
    {
	return "BASKET";
    }

    
}
