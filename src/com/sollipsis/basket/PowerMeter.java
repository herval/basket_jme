
package com.sollipsis.basket;

import javax.microedition.lcdui.Graphics;
import javax.microedition.lcdui.Image;

import com.sollipsis.sgf.util.ImageLoader;

import com.sollipsis.basket.config.Layout;


public class PowerMeter
{
    private int				power;

    private static final String		POWER_DISP_IMG_NAME = "/power_display.png";
    private Image			powerDispImg;

    private int				xBar;
    private int				yBar;
    private int				wBar;
    private int				hBar;


    public PowerMeter()
    {
	this.power = 0;
	powerDispImg = ImageLoader.loadImage(POWER_DISP_IMG_NAME);

	xBar = - powerDispImg.getWidth();
	wBar = 2 * powerDispImg.getWidth();

	yBar =  Layout.POWER_DISPLAY_EY - powerDispImg.getHeight();
	hBar = 2 * powerDispImg.getHeight();
    }

    public void paint(Graphics g)
    {
	g.setColor(0, 255, 0);
	g.fillArc(xBar, yBar, wBar, hBar, 0, power);

	g.drawImage(powerDispImg, Layout.POWER_DISPLAY_X, Layout.POWER_DISPLAY_EY,
		    Graphics.LEFT | Graphics.BOTTOM);
    }
}
