//***********************************
// File    : BasketCanvas.java
// Created : 2003-06-24
// Modified: 2003-07-08
// 
// (c) 2003, Sollipsis Interactive
//***********************************

package com.sollipsis.basket;

import javax.microedition.lcdui.Graphics;
import javax.microedition.lcdui.Image;

import com.sollipsis.sgf.GameCanvas;
import com.sollipsis.sgf.util.ImageLoader;


public class BasketCanvas extends GameCanvas
{
    private Ball		ball = new Ball();

    private PowerMeter		powerMeter = new PowerMeter();

    private Basket		basket = new Basket();

    private Player		player1 = Players.galegoMan;

    private BallPointer		horPointer;
    private BallPointer		vertPointer;

    private static final String BKG_IMG_NAME = "/background.png";
    private Image		bkgImg;


    public BasketCanvas()
    {
	loadBackground();
	horPointer = new BallPointer(BallPointer.ORIENTATION_HORIZONTAL, player1.getSkill());
	vertPointer = new BallPointer(BallPointer.ORIENTATION_VERTICAL, player1.getSkill());

	addSprite(horPointer);
	addSprite(vertPointer);
	addSprite(basket);
    }

    protected void paintBackground(Graphics g)
    {
	g.drawImage(bkgImg, 0, 0, Graphics.TOP | Graphics.LEFT);
	powerMeter.paint(g);
	player1.paint(g);
    }


    //--- metodos de implementacao ---
    private void loadBackground()
    {
	bkgImg = ImageLoader.loadImage(BKG_IMG_NAME);
    }
}
