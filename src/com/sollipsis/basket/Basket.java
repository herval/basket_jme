//***********************************
// File    : Basket.java
// Created : 2003-06-24
// Modified: 2003-07-08
// 
// (c) 2003, Sollipsis Interactive
//***********************************

package com.sollipsis.basket;

import javax.microedition.lcdui.Graphics;
import javax.microedition.lcdui.Image;

import com.sollipsis.sgf.BasicSprite;
import com.sollipsis.sgf.util.ImageLoader;

import com.sollipsis.basket.config.Layout;


/**
 * A cesta de basquete.
 *
 * @author Andrei de A. Formiga
 */
public class Basket extends BasicSprite
{
    private static final String[] IMAGE_NAMES = { "/basket.png" };

    private static final int	STATE_NORMAL = 0;
    private static final int	STATE_WITHBALL = 1;
    private static final int	STATE_SWINGING = 2;

    public Basket()
    {
	super(Layout.BASKET_X, Layout.BASKET_Y, Layout.BASKET_W, Layout.BASKET_H,
	      IMAGE_NAMES);
    }
}
