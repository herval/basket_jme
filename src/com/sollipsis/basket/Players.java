//***********************************
// File    : Players.java
// Created : 2003-06-24
// Modified: 2003-06-24
// 
// (c) 2003, Sollipsis Interactive
//***********************************

package com.sollipsis.basket;


/**
 * Re�ne os objetos dos jogadores dispon�veis.
 *
 * @author Andrei de A. Formiga
 */
public class Players
{

    public static final Player		galegoMan = new Player(3, 3, 
							       "/galego_stand.png",
							       "/galego_ready.png",
							       "/galego_throw.png");

    public static final Player		blackMan = new Player(5, 1, 
							       "/black_stand.png",
							       "/black_ready.png",
							       "/black_throw.png");

    public static final Player		japaMan = new Player(1, 5, 
							       "/japa_stand.png",
							       "/japa_ready.png",
							       "/japa_throw.png");
}
