//***********************************
// File    : BallPointer.java
// Created : 2003-06-24
// Modified: 2003-06-25
// 
// (c) 2003, Sollipsis Interactive
//***********************************

package com.sollipsis.basket;

import com.sollipsis.sgf.Sprite;

import com.sollipsis.basket.config.Layout;


public class BallPointer extends Sprite
{
    //--- constantes ---
    public static final int		ORIENTATION_HORIZONTAL = 0;
    public static final int		ORIENTATION_VERTICAL = 1;

    private static final int		SKILL_SPEED_MULTIPLIER = 2;

    private static final String[]	imageNames = { "/pointer_hor.png", "/pointer_vert.png" };


    //--- variaveis de instancia ---
    private int				orientation;

    private int				location;


    //--- interface publica ---
    public BallPointer(int orientation, int skill)
    {
	super(Layout.VERTICAL_POINTER_W, Layout.HORIZONTAL_POINTER_H, imageNames);

	this.orientation = orientation;

	// as constantes de orientacao tambem sao usadas como n�meros de sequencia
	setSequence(orientation);

	if (orientation == ORIENTATION_HORIZONTAL)
	{
	    y = Layout.HORIZONTAL_POINTER_Y;
	    x = Layout.HORIZONTAL_POINTER_MIN_X;
	    vx = (Player.SKILL_MAX - skill + 1) * SKILL_SPEED_MULTIPLIER;
	}
	else
	{
	    x = Layout.VERTICAL_POINTER_X;
	    y = Layout.VERTICAL_POINTER_MIN_Y;
	    vy = (Player.SKILL_MAX - skill + 1) * SKILL_SPEED_MULTIPLIER;
	}
    }

    /**
     * P�ra o movimento do ponteiro, guardando sua localiza��o
     */
    public void mark()
    {
	if (orientation == ORIENTATION_HORIZONTAL)
	    vx = 0;
	else 
	    vy = 0;
    }


    //--- metodos reimplementados ---
    protected void updateHook(long elapsedTimeMillis, int nPeriods)
    {
	if (orientation == ORIENTATION_HORIZONTAL)
	{
	    if (x < Layout.HORIZONTAL_POINTER_MIN_X)
	    {
		x = Layout.HORIZONTAL_POINTER_MIN_X;
		vx = - vx;
	    }
	    if (x > (Layout.HORIZONTAL_POINTER_MAX_X - Layout.HORIZONTAL_POINTER_W))
	    {
		x = Layout.HORIZONTAL_POINTER_MAX_X - Layout.HORIZONTAL_POINTER_W;
		vx = - vx;
	    }
	}
	else
	{
	    if (y < Layout.VERTICAL_POINTER_MIN_Y)
	    {
		y = Layout.VERTICAL_POINTER_MIN_Y;
		vy = - vy;
	    }
	    if (y > (Layout.VERTICAL_POINTER_MAX_Y - Layout.VERTICAL_POINTER_H))
	    {
		y = Layout.VERTICAL_POINTER_MAX_Y - Layout.VERTICAL_POINTER_H;
		vy = - vy;
	    }
	}
    }
}

