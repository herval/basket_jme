# j2me_postbuild.py
# Script pos-compilacao para projetos J2ME
# Andrei de A. Formiga, 11/02/2003

# Funcao basica: constroi o arquivo .jad a partir do MANIFEST.MF e do jar correspondente

import sys
import os

### Constantes ###
NEW_LINE = '\n'

if (len(sys.argv) < 2):
    print "Uso: python j2me_postbuild.py <nome>"
    sys.exit(0)

projectName = sys.argv[1]
jarName = projectName + ".jar"
jadName = projectName + ".jad"

manifest = open("MANIFEST.MF")
manifestContents = manifest.readlines()
manifest.close()

jarSize = os.stat(projectName + ".jar").st_size

manifestContents.append("MIDlet-Jar-Size: " + str(jarSize) + NEW_LINE)
manifestContents.append("MIDlet-Jar-URL: " + str(jarName) + NEW_LINE)

jadFile = open(jadName, "w")
jadFile.writelines(manifestContents)
jadFile.close()


