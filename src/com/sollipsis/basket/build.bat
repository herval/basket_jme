@echo off

rem Build script para aplicacoes MIDP

mkdir tmpclasses
mkdir classes

echo *** Compilando classes ***

rem *** generico ***
rem javac -g:none -bootclasspath D:\java\WTK104\lib\midpapi.zip -d tmpclasses -classpath -classpath D:\sollipsis\sgf\src;D:\sollipsis\sgf\src\com\sollipsis\sgf\config\nokia_s60;D:\sollipsis\basket\src\com\sollipsis\basket\config\generic;tmpclasses *.java


rem *** Serie 40 ***
rem javac -g:none -bootclasspath D:\Nokia\Devices\Nokia_Series_40_MIDP_Concept_SDK_Beta_0_3\lib\classes.zip -d tmpclasses -classpath D:\sollipsis\sgf\src;D:\sollipsis\sgf\src\com\sollipsis\sgf\config\nokia_s60;D:\sollipsis\basket\src\com\sollipsis\basket\config\nokia_s40;tmpclasses *.java


rem *** Serie 60 ***
javac -g:none -bootclasspath D:\Nokia\Devices\Nokia_Series_60_MIDP_Concept_SDK_Beta_0_2\lib\classes.zip -d tmpclasses -classpath D:\sollipsis\sgf\src;D:\sollipsis\sgf\src\com\sollipsis\sgf\config\nokia_s60;D:\sollipsis\basket\src\com\sollipsis\basket\config\nokia_s60;tmpclasses *.java


echo *** Preverificando arquivos .class ***

rem *** generico ***
rem D:\java\WTK104\bin\preverify -classpath D:\java\WTK104\lib\midpapi.zip;.\tmpclasses -d classes tmpclasses

rem *** Serie 40 ***
rem D:\Nokia\Devices\Nokia_Series_40_MIDP_Concept_SDK_Beta_0_3\bin\preverify -classpath D:\Nokia\Devices\Nokia_Series_40_MIDP_Concept_SDK_Beta_0_3\lib\classes.zip;.\tmpclasses -d classes tmpclasses

rem *** Serie 60 ***
D:\Nokia\Devices\Nokia_Series_60_MIDP_Concept_SDK_Beta_0_2\bin\preverify -classpath D:\Nokia\Devices\Nokia_Series_60_MIDP_Concept_SDK_Beta_0_2\lib\classes.zip;.\tmpclasses -d classes tmpclasses

echo *** Criando o jar ***
jar cmf MANIFEST.MF Basket.jar -C classes .
jar umf MANIFEST.MF Basket.jar -C ..\..\..\..\res\nokia_s60 .

echo *** Criando o jad ***
python j2me_postbuild.py Basket

