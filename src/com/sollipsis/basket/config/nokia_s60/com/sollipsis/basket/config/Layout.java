//***********************************
// File    : Layout.java
// Created : 2003-06-24
// Modified: 2003-06-24
// 
// (c) 2003, Sollipsis Interactive
//***********************************

package com.sollipsis.basket.config;


public class Layout
{
    // localizacao do jogador
    public static final int		PLAYER_X = 64;
    public static final int		PLAYER_EY = 208;

    // localizacao o medidor de forca
    public static final int		POWER_DISPLAY_X = 0;
    public static final int		POWER_DISPLAY_EY = 208;

    // localizacao da cesta
    public static final int		BASKET_X = 73;
    public static final int		BASKET_Y = 100;
    public static final int		BASKET_W = 29;
    public static final int		BASKET_H = 33;

    // localizacao da mira horizontal
    public static final int		HORIZONTAL_POINTER_Y = 22;
    public static final int		HORIZONTAL_POINTER_MIN_X = 24;
    public static final int		HORIZONTAL_POINTER_MAX_X = 150;
    public static final int		HORIZONTAL_POINTER_W = 28;
    public static final int		HORIZONTAL_POINTER_H = 38;

    // localizacao da mira vertical
    public static final int		VERTICAL_POINTER_X = 136;
    public static final int		VERTICAL_POINTER_MIN_Y = 30;
    public static final int		VERTICAL_POINTER_MAX_Y = 124;
    public static final int		VERTICAL_POINTER_W = 38;
    public static final int		VERTICAL_POINTER_H = 28;


    // dimensoes da bola
    public static final int		BALL_W = 20;
    public static final int		BALL_H = 20;
}
